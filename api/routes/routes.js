var express = require('express');
var router = express.Router();


var userController = require('../controllers/user');
var advertisementsController = require('../controllers/advertisements');
var paymentsController = require('../controllers/payments');
var commentsController = require('../controllers/comments');

//Routes for User Registration
//#1
router.route('/register')
  .post(userController.postUsers);

//#2
router.route('/login')
  .post(userController.login);

//#3
router.route('/users')
  .get(userController.getUsers);
router.route('/users/:user_id')
  .get(userController.getUser)
  .put(userController.putUser)
  .delete(userController.deleteUser);

//Routes for advertisements Registration
//#1
router.route('/advertisements')
  .post(advertisementsController.postAdvertisements);
router.route('/advertisements')
  .get(advertisementsController.getAdvertisements);

router.route('/advertisements/:advertisement_id')
  .get(advertisementsController.getAdvertisement)
  .put(advertisementsController.putAdvertisement)
  .delete(advertisementsController.deleteAdvertisement);


//Routes for Payments Registration
//#1
router.route('/payments')
  .post(paymentsController.postPayments);
router.route('/payments')
  .get(paymentsController.getPayments);
router.route('/payments/:payment_id')
  .get(paymentsController.getPayment)
  .put(paymentsController.putPayment)
  .delete(paymentsController.deletePayment);


  //Routes for Comments Registration
//#1
router.route('/comments')
  .post(commentsController.postComments);
router.route('/comments')
  .get(commentsController.getComments)
router.route('/comments/:comment_id')
  .get(commentsController.getComment)
  .put(commentsController.putComment)
  .delete(commentsController.deleteComment);
  
module.exports = router;