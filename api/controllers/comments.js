// Load required packages
var Comments = require('../models/comments');

    
// Create endpoint /api/comments for POST
exports.postComments = function(req, res) {
  // Create a new instance of the User model
  var commentData = req.body.comment;
  
        var comment = new Comments();

        // Set the comment properties that came from the POST data
        comment.ID = (commentData.ID !== undefined)? commentData.ID : '';
        comment.user_ID = (commentData.user_ID !== undefined)? commentData.user_ID : '';
        comment.AD_ID = (commentData.AD_ID !== undefined)? commentData.AD_ID : '';
        comment.Description = (commentData.Description !== undefined)? commentData.Description : '';
        comment.Date_time = (commentData.Date_time !== undefined)? commentData.Date_time : '';
        comment.Type = (commentData.Type !== undefined)? commentData.Type : '';
        comment.Rating = (commentData.Rating !== undefined)? commentData.Rating : '';
        
  
        // Save the comment and check for errors
        comment.save(function(err) {
          if (err)
          return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
          return res.status(200).json({
	  			    data: comment,
	  			    message: "Comment successfully created"
	  		  });
        });
      };

// Create endpoint /api/comments/:comment_id for GET
exports.getComment = function(req, res) {
  // Use the Comment model to find a specific comment
  Comments.find({ _id: req.params.comment_id }, function(err, comment) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (comment.length > 0) {
      return res.status(200).json({
	  			    data: comment,
	  			    message: "Comment found"
	  		  });
    }else {
      return res.status(400).json({data : comment, message : 'comment not found!'});
    } 
        
  });
};

// Create endpoint /api/comments/:comment_id for GET
exports.getComments = function(req, res) {
  // Use the Comment model to find a specific comment
  Comments.find({}, function(err, comment) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (comment.length > 0) {
      return res.status(200).json({
	  			    data: comment,
	  			    message: "Comments found"
	  		  });
    }else {
      return res.status(400).json({data : comment, message : 'comments not found!'});
    } 
        
  });
};

// Create endpoint /api/comments/:comment_id for PUT
exports.putComment = function(req, res) {
  
  var comment = req.body.comment;
  
  // Use the Comment model to find a specific comment
  Comments.findOneAndUpdate({ _id: req.params.comment_id },{$set:comment}, {new: true},function(err, commentn, raw) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});

    if(!!commentn)
      return res.status(200).json({
	  			    data: commentn,
	  			    message: "Comment Updated Successfully"
	  		  });
    else
      return res.status(400).json({data : commentn, message : 'comment not found!'});
    
  });
};

// Create endpoint /api/comments/:comment_id for DELETE
exports.deleteComment = function(req, res) {
  // Use the Comment model to find a specific comment and remove it
  Comments.remove({ _id: req.params.comment_id }, function(err,num,raw) {
    if (err)
      return res.send(err);
    
    if(num.result.n != 0)
      return res.status(200).json({
	  			    data: num,
	  			    message: "Comment removed from Comments!"
	  		  });
    else
      return res.status(400).json({data : num, message : 'Comment not found!'});
      
  });
};
