// Load required packages
var Payments = require('../models/payments');


    
// Create endpoint /api/payments for POST
exports.postPayments = function(req, res) {
  
  var PaymentData = req.body.payment;

  // Create a new instance of the Payments model
  
        var payment = new Payments();

        // Set the user properties that came from the POST data
        payment.Transaction_ID = (PaymentData.Transaction_ID !== undefined)? PaymentData.Transaction_ID : '';
        payment.Date_Time = (PaymentData.Date_Time !== undefined)? PaymentData.Date_Time : '';
        payment.From_User_ID = (PaymentData.From_User_ID !== undefined)? PaymentData.From_User_ID : '';
        payment.To_User_ID = (PaymentData.To_User_ID !== undefined)? PaymentData.To_User_ID : '';
        payment.Description = (PaymentData.Description !== undefined)? PaymentData.Description : '';
        payment.From_bank_account = (PaymentData.From_bank_account !== undefined)? PaymentData.From_bank_account : '';
        payment.To_bank_account = (PaymentData.To_bank_account !== undefined)? PaymentData.To_bank_account : '';
        payment.amount = (PaymentData.amount !== undefined)? PaymentData.amount : '';
        payment.currency = (PaymentData.currency !== undefined)? PaymentData.currency : '';
        payment.Invoice = (PaymentData.Invoice !== undefined)? PaymentData.Invoice : '';
               
  
        // Save the user and check for errors
        payment.save(function(err) {
          if (err)
          return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
          return res.status(200).json({
	  			    data: payment,
	  			    message: "Payment successfully created"
	  		  });
        });
      };

// Create endpoint /api/payments/:payment_id for GET
exports.getPayment = function(req, res) {
  // Use the Payment model to find a specific user
  Payments.find({ _id: req.params.payment_id }, function(err, payment) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (payment.length > 0) {
      return res.status(200).json({
	  			    data: payment,
	  			    message: "Payment found"
	  		  });
    }else {
      return res.status(400).json({data : payment, message : 'payment not found!'});
    } 
        
  });
};

// Create endpoint /api/payments for GETTing All
exports.getPayments = function(req, res) {
  // Use the Payment model to find a specific user
  Payments.find({}, function(err, payment) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (payment.length > 0) {
      return res.status(200).json({
	  			    data: payment,
	  			    message: "Payments found"
	  		  });
    }else {
      return res.status(400).json({data : payment, message : 'payments not found!'});
    } 
        
  });
};

// Create endpoint /api/payments/:payment_id for PUT
exports.putPayment = function(req, res) {
  
  var payment = req.body.payment;
  
  // Use the Payment model to find a specific payment
  Payments.findOneAndUpdate({ _id: req.params.payment_id },{$set:payment}, {new: true}, function(err, paymentn, raw) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});

    if(!!paymentn)
      return res.status(200).json({
	  			    data: paymentn,
	  			    message: "Payment Updated Successfully"
	  		  });
    else
      return res.status(400).json({data : paymentn, message : 'payment not found!'});
    
  });
};

// Create endpoint /api/payments/:payment_id for DELETE
exports.deletePayment = function(req, res) {
  // Use the Payment model to find a specific payment and remove it
  Payments.remove({ _id: req.params.payment_id }, function(err,num,raw) {
    if (err)
      return res.send(err);
    
    if(num.result.n != 0)
      return res.status(200).json({
	  			    data: num,
	  			    message: "Payment removed from Payments!"
	  		  });
    else
      return res.status(400).json({data : num, message : 'Payment not found!'});
      
  });
};
