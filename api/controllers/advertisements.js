// Load required packages
var Advertisements = require('../models/advertisements');

var bcrypt = require('bcryptjs');

    
// Create endpoint /api/Advertisement for POST
exports.postAdvertisements = function(req, res) {
  // Create a new instance of the User model
  var advertisementsData = req.body.advertisement;
  
        var advertisement = new Advertisements();

        // Set the Advertisement properties that came from the POST data
        advertisement.Type = (advertisementsData.Type !== undefined)? advertisementsData.Type : '';
        advertisement.Header = (advertisementsData.Header !== undefined)? advertisementsData.Header : '';
        advertisement.description = (advertisementsData.description !== undefined)? advertisementsData.description : '';
        advertisement.Owner_user_ID = (advertisementsData.Owner_user_ID !== undefined)? advertisementsData.Owner_user_ID : '';
        advertisement.Units_available = (advertisementsData.Units_available !== undefined)? advertisementsData.Units_available : '';
        advertisement.start_time = (advertisementsData.start_time !== undefined)? advertisementsData.start_time : '';
        advertisement.end_time = (advertisementsData.end_time !== undefined)? advertisementsData.end_time : '';
        advertisement.ad_duration = (advertisementsData.ad_duration !== undefined)? advertisementsData.ad_duration : '';
        advertisement.Rate = (advertisementsData.Rate !== undefined)? advertisementsData.Rate : '';
        advertisement.currency = (advertisementsData.currency !== undefined)? advertisementsData.currency : '';
        advertisement.scale = (advertisementsData.scale !== undefined)? advertisementsData.scale : '';
        advertisement.Length = (advertisementsData.Length !== undefined)? advertisementsData.Length : '';
        advertisement.width = (advertisementsData.width !== undefined)? advertisementsData.width : '';
        advertisement.Height = (advertisementsData.Height !== undefined)? advertisementsData.Height : '';
        advertisement.Elevation = (advertisementsData.Elevation !== undefined)? advertisementsData.Elevation : '';
        advertisement.Illuminated = (advertisementsData.Illuminated !== undefined)? advertisementsData.Illuminated : '';
        advertisement.license_copy = (advertisementsData.license_copy !== undefined)? advertisementsData.license_copy : '';
        advertisement.geo_location = (advertisementsData.geo_location !== undefined)? advertisementsData.geo_location : '';
        advertisement.photos = (advertisementsData.photos !== undefined)? advertisementsData.photos : '';
        
  
        // Save the Advertisement and check for errors
        advertisement.save(function(err) {
          if (err)
          return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
          return res.status(200).json({
	  			    data: advertisement,
	  			    message: "Advertisement successfully created"
	  		  });
        });
      };

// Create endpoint /api/advertisements for GET All
exports.getAdvertisements = function(req, res) {
  // Use the Advertisement model to find a specific Advertisement
  Advertisements.find({}, function(err, advertisement) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (advertisement.length > 0) {
      return res.status(200).json({
	  			    data: advertisement,
	  			    message: "Advertisements found"
	  		  });
    }else {
      return res.status(400).json({data : advertisement, message : 'Advertisements not found!'});
    } 
        
  });
};

// Create endpoint /api/advertisements/:advertisement_id for GET
exports.getAdvertisement = function(req, res) {
  // Use the Advertisement model to find a specific Advertisement
  Advertisements.find({ _id: req.params.advertisement_id }, function(err, advertisement) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (advertisement.length > 0) {
      return res.status(200).json({
	  			    data: advertisement,
	  			    message: "Advertisement found"
	  		  });
    }else {
      return res.status(400).json({data : advertisement, message : 'advertisement not found!'});
    } 
        
  });
};

// Create endpoint /api/advertisements/:advertisement_id for PUT
exports.putAdvertisement = function(req, res) {
  
  var advertisement = req.body.advertisement;
  
  // Use the Advertisement model to find a specific advertisement
  Advertisements.findOneAndUpdate({ _id: req.params.advertisement_id },{$set:advertisement}, {new: true}, function(err, advertisementn) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});

    if(!!advertisementn)
      return res.status(200).json({
	  			    data: advertisementn,
	  			    message: "Advertisement Updated Successfully"
	  		  });
    else
      return res.status(400).json({data : advertisementn, message : 'advertisement not found!'});
    
  });
};

// Create endpoint /api/advertisements/:advertisement_id for DELETE
exports.deleteAdvertisement = function(req, res) {
  // Use the Advertisement model to find a specific advertisement and remove it
  Advertisements.remove({ _id: req.params.advertisement_id }, function(err,num,raw) {
    if (err)
      return res.send(err);
    
    if(num.result.n != 0)
      return res.status(200).json({
	  			    data: num,
	  			    message: "Advertisement removed from Advertisements!"
	  		  });
    else
      return res.status(400).json({data : num, message : 'Advertisement not found!'});
      
  });
};
