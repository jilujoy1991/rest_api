// Load required packages
var User = require('../models/user');

var bcrypt = require('bcryptjs');

    
// Create endpoint /api/users for POST
exports.postUsers = function(req, res) {
  // Create a new instance of the User model
  var userData = req.body.user;
  User.findOne({ username: userData.username }, function(err, user) {
    	if (err)
      			//return res.send(err);
				return res.status(400).json({data : {error: err}, message : 'Register Failed!'});
    		
			if (user){
        return res.status(400).json({ message : 'Username already taken'});
      }else{
        var user = new User();

        // Set the user properties that came from the POST data
        user.firstName = (userData.firstName !== undefined)? userData.firstName : '';
        user.lastName = (userData.lastName !== undefined)? userData.lastName : '';
        user.gender = (userData.gender !== undefined)? userData.gender : '';
        user.mobile = (userData.mobile !== undefined)? userData.mobile : '';
        user.type = (userData.type !== undefined)? userData.type : '';
        user.address = (userData.address !== undefined)? userData.address : '';
        user.IDproof = (userData.IDproof !== undefined)? userData.IDproof : '';
        user.carddetails = (userData.carddetails !== undefined)? userData.carddetails : '';
        user.TIN = (userData.TIN !== undefined)? userData.TIN : '';
        user.email = (userData.email !== undefined)? userData.email : '';
        user.username = (userData.username !== undefined)? userData.username : '';
        userData.password = (userData.password !== undefined)? userData.password : '';
        user.hash = bcrypt.hashSync(userData.password, 10);

  
        // Save the user and check for errors
        user.save(function(err) {
          if (err)
          return res.status(400).json({data : {error: err}, message : 'Register Failed!'});
    
          return res.status(200).json({
	  			    data: user,
	  			    message: "Account successfully created"
	  		  });
        });
      }
       
  });
  
};



// Create endpoint /api/users for Login
exports.login = function(req, res) {
  var user = req.body.user;
  console.log(user);
  if (user.username == undefined)
    return res.status(400).json({message : 'Enter Username'});
  if (user.password == undefined)
    return res.status(400).json({message : 'Enter Password'});
  var username = (user.username !== undefined)? user.username : '';
  var password = (user.password !== undefined)? user.password : '';
  
  
  User.findOne({ username: username }, function(err, user) {
    	if (err)
      			//return res.send(err);
				return res.status(400).json({data : {error: err}, message : 'Login Failed!'});
    		
			if (!user)
				return res.status(400).json({message : 'Invalid Username'});
      if (!user.validPassword(password))
        return res.status(400).json({ message : 'Oops! Wrong password.'});
			else
				return res.status(200).json({data : user, message : 'Login Successfull'});
        
  });
  
};

// Create endpoint /api/users for GET All Users
exports.getUsers = function(req, res) {
  // Use the User model to find a users
  User.find({}, function(err, user) {
    if (err)
       return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (user.length > 0) {
      return res.status(200).json({
	  			    data: user,
	  			    message: "Users found"
	  		  });
    }else {
      return res.status(400).json({data : user, message : 'User not found!'});
    }    
  });
};

// Create endpoint /api/users/:user_id for GET
exports.getUser = function(req, res) {
  // Use the User model to find a specific user
  User.find({ _id: req.params.user_id }, function(err, user) {
    if (err)
       return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if (user.length > 0) {
      return res.status(200).json({
	  			    data: user,
	  			    message: "User found"
	  		  });
    }else {
      return res.status(400).json({data : user, message : 'User not found!'});
    }    
  });
};

// Create endpoint /api/users/:user_id for PUT
exports.putUser = function(req, res) {
  
  var user = req.body.user;
  user.hash = bcrypt.hashSync(user.password, 10);
  // Use the User model to find a specific user
  User.findOneAndUpdate({ _id: req.params.user_id },{$set:user}, {new: true}, function(err, usern, raw) {
    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});

    if(!!usern)
      return res.status(200).json({
	  			    data: usern,
	  			    message: "User Updated Successfully"
	  		  });
    else
      return res.status(400).json({data : usern, message : 'User not found!'});

  });
};

// Create endpoint /api/users/:user_id for DELETE
exports.deleteUser = function(req, res) {
  // Use the User model to find a specific user and remove it
  User.remove({ _id: req.params.user_id }, function(err,num,raw) {

    if (err)
      return res.status(400).json({data : {error: err}, message : 'Transaction Failed!'});
    
    if(num.result.n != 0)
      return res.status(200).json({
	  			    data: num,
	  			    message: "User removed from Users!"
	  		  });
    else
      return res.status(400).json({data : num, message : 'User not found!'});
  });
};