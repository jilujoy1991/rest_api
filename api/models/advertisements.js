var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var advertisementsSchema = mongoose.Schema({
	Type:{
		type: Number,
		enum: [1,2,3,4,5,null]
	},
    Header:{
		type: String
	},
	description:{
		type: String
	},
	Owner_user_ID:{
		type: String
	},
	Units_available:{
		type: String
	},
	start_time:{
		type: String
	},
	end_time:{
		type: String
	},
	ad_duration:{
		type: String
	},
	Rate:{
		type: String
	},
    currency:{
		type: String
	},
    scale:{
		type: String,
		enum: ['inches', 'meters','seconds','']
	},
    Length:{
		type: String
	},
    width:{
		type: String
	},
    Height:{
		type: String
	},
    Elevation:{
		type: String
	},
    Illuminated:{
		type: String,
		enum: ['Yes', 'no','Partially','']
	},
    license_copy:{
		type: String
	},
    geo_location:{
		type: String
	},
    photos:{
		type: String
	}
});

var Advertisements = module.exports = mongoose.model('Advertisements', advertisementsSchema);