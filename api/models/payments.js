var mongoose = require('mongoose');

// Payments Schema
var paymentsSchema = mongoose.Schema({
	Transaction_ID:{
		type: String
	},
    Date_Time:{
		type: String
	},
	From_User_ID:{
		type: String
	},
	To_User_ID:{
		type: String
	},
	Description:{
		type: String
	},
	From_bank_account:{
		type: String
	},
	To_bank_account:{
		type: String
	},
	amount:{
		type: String
	},
	currency:{
		type: String
	},
    Invoice:{
		type: String
	}
});

var Payments = module.exports = mongoose.model('Payments', paymentsSchema);