var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var _ = require('lodash');

// User Schema
var userSchema = mongoose.Schema({
	firstName:{
		type: String
	},
    lastName:{
		type: String
	},
	gender:{
		type: String
	},
	mobile:{
		type: String
	},
	type:{
		type: String
	},
	address:{
		type: String
	},
	IDproof:{
		type: String
	},
	carddetails:{
		type: String
	},
	TIN:{
		type: String
	},
    email:{
		type: String
	},
    username:{
		type: String
	},
    hash:{
		type: String
	}
});
// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.hash);
};

var User = module.exports = mongoose.model('User', userSchema);