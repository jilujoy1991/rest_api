var mongoose = require('mongoose');

// Comments Schema
var commentsSchema = mongoose.Schema({
	ID:{
		type: String
	},
    user_ID:{
		type: String
	},
	AD_ID:{
		type: String
	},
	Description:{
		type: String
	},
	Date_time:{
		type: String
	},
	Type:{
		type: String
	},
	Rating:{
		type: String
	}
});

var Comments = module.exports = mongoose.model('Comments', commentsSchema);