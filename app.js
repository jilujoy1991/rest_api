var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');
var routes = require('./api/routes/routes');
var config = require('./config');
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/RESTAPI');
var db = mongoose.connection;
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'example.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}
app.use(allowCrossDomain);
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



app.get('/test', function(req, res){
	res.send('Successfully connected to AdbBello API');
});

app.use('/api', routes);

app.use(function(req, res) {
    var ret = {"error" : "route not found"};
    res.send(ret);
});

app.listen(3000);
console.log('Running on port 3000...');